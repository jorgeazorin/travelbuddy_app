import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import User from '../../models/user.model';
import swal from 'sweetalert2'
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router : Router) {
  }
  ngOnInit() {
  }

  email = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  password = new FormControl('', [
    Validators.required
  ]);



  login(){

    var user : User = new User;
    user.email = this.email.value;
    user.password = this.password.value;

    this.userService.login(user).subscribe(
      data => {
        localStorage.setItem('session', JSON.stringify(data));
        this.router.navigateByUrl('/home');
        
      },err =>{
        console.error(err);
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'Login incorrecto!',
          footer: '<a href>Nombre de usuario o contraseña incorrectos?</a>',
        })

      }
    );
  }



}
