import { Component, OnInit } from '@angular/core';
import Meeting from '../../models/meeting.model';
import { MeetingService } from '../../services/meeting.service';
import User from '../../models/user.model';

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.css']
})
export class MeetingsComponent implements OnInit {

  meetings:Meeting[]
  me: User

  constructor(private meetingService: MeetingService ) { }

  ngOnInit() {
    this.me = JSON.parse(localStorage.getItem('session')).user;

    this.meetingService.getMeetings({}).subscribe(
      data=>this.meetings = data
    )

  }
}