import { Component, OnInit } from '@angular/core';
import { MeetingService } from '../../services/meeting.service';
import Meeting from '../../models/meeting.model';
import User from '../../models/user.model';
import { MessageService } from '../../services/message.service';
import Message from '../../models/message.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  meetings:Meeting[]
  me: User
  chats: Message[]

  constructor(private meetingService: MeetingService, private messageService: MessageService) { }

  ngOnInit() {
    this.me = JSON.parse(localStorage.getItem('session')).user;

    this.meetingService.getMeetings({}).subscribe(
      data=>this.meetings = data
    )
    this.messageService.getMessages({chats:true}).subscribe(
      data => { this.chats = data },
      err => console.error(err),
    );

  }

}
