import { Component, OnInit } from '@angular/core';
import { MeetingService } from '../../services/meeting.service';
import { UserService } from '../../services/user.service';
import Meeting from '../../models/meeting.model';
import User from '../../models/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import MeetingImage from '../../models/meetingImage.model';
import swal from 'sweetalert2'
import Session from '../../models/session.model';
import { CommentService } from '../../services/comment.service';
import { BuddyService } from '../../services/buddy.service';



@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  meeting: Meeting
  user: User
  loaded: Boolean = false
  editing: Boolean = false
  meetingImages: MeetingImage[]
  session: Session;
  rate: number
  constructor(private meetingService: MeetingService, private userService: UserService,private router: Router, private route: ActivatedRoute) { }

  update = function() {
    this.meetingService.updateMeeting(this.meeting).subscribe(
      data => {
        this.meeting = data
        this.editing = false
      }
    )
  }


  parse(a) {
    return JSON.parse(a);
  }
  
  ngDoCheck() {
    if(this.meeting && this.meeting.rate != this.rate){
      this.meeting.rate = this.rate;
        this.updateRate();
    }
  }

  delete = function() {
    this.meetingService.deleteMeeting(this.meeting.id).subscribe(
      data => {
        this.router.navigateByUrl('/meetings');
      }
    )
  }

  ngOnInit() {
    this.session = JSON.parse(localStorage.getItem('session'));    
    this.route.params.subscribe(params => {
      var id = +params['id'];   
      this.meetingService.getMeetings({id: id}).subscribe(
        data=>{
          this.meeting = data[0]
          let idUser = this.meeting.buddy.user.id;
          this.rate = this.meeting.rate;
          
          if(this.meeting.buddy.user.id == this.session.user.id){
            idUser = this.meeting.user.id
          }
          this.userService.getUsers({id:idUser}).subscribe(
            data => {
              this.user = data[0]
              this.loaded = true;
            }
          )
        }
      )
      this.loadImages(id);
      
    })  
  }

  loadImages(id ){
    this.meetingService.getMeetingImages(id).subscribe(
      data=>{
        this.meetingImages = data
      }
    )
  }


  selectedFile: File;
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];

    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      var img :any= document.querySelector("#preview img");
      img.file = file;
      var reader = new FileReader();
      reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
      reader.readAsDataURL(file);
    }
   }


   updateRate(){
     this.meetingService.updateSocore(this.meeting).subscribe(
        data =>{
          swal({
            type: 'success',
            title: '¡Bien!',
            text: '¡Has puntuado la quedada con el Buddy!'
          })
        }
     )
   }
   guardarFoto(){

    var meetingImage: MeetingImage = new MeetingImage();
    meetingImage.meeting = this.meeting;
    let that = this;
    this.meetingService.uploadMeetingImage(meetingImage,this.selectedFile).subscribe(
      data=>{
        swal({
          type: 'success',
          title: '¡Bien!',
          text: '¡La imagen se ha subido correctamente!'
        })
        that.selectedFile = null;
        that.loadImages(this.meeting.id);
        var img :any= document.querySelector("#preview img");
        img.file = null;
        img.src = null;
      }
    );
   }

}
