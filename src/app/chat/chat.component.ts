import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';
import Message from '../../models/message.model';
import { ActivatedRoute } from '@angular/router';
import Session from '../../models/session.model'
import User from '../../models/user.model';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private stompClient;

  id: number;
  messages : Message[]
  session: Session
  user: User
  message: string = ''
  loaded: Boolean = false;


  me: User
  
  constructor(private route: ActivatedRoute ,private userService: UserService, private messageService: MessageService) {
    this.initializeWebSocketConnection();
   }

  send = function(){
    let message : Message = new Message();
    message.to = this.user;
    message.text = this.message;
    this.messageService.createMessage(message).subscribe(
      data=>{
        this.messages.push(data);
        setTimeout(() => {
          window.scrollTo(0,document.body.scrollHeight);
        }, 100);
      }
    );


  }
  
  load = function(){
    this.messageService.getMessages({chatId: this.id}).subscribe(
      data=>{
        this.messages = data
        setTimeout(() => {
          window.scrollTo(0,document.body.scrollHeight);
        }, 100);
      }
    )

    this.message = ''
    this.loaded = true
  }


  ngOnInit() {
    this.session = JSON.parse(localStorage.getItem('session'));
    this.route.params.subscribe(params => {
      this.id = +params['id'];     
      this.userService.getUsers({id:this.id}).subscribe(
        data=>{
          this.user = data[0]
          this.load();
        }
      )
      
      })
  };


  



  initializeWebSocketConnection(){
    let ws = new SockJS("http://localhost:8080/socket");
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe("/wschat/chat"+that.session.user.id, (message) => {
        if(message.body) {
          if(message.body.to == that.id){
            that.messages.push(JSON.parse(message.body));        
          }
        }
      });
    });
  }

}
