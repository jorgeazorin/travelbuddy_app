import { Component, OnInit } from '@angular/core';
import { BuddyService } from '../../services/buddy.service';
import { MessageService } from '../../services/message.service';
import Message from '../../models/message.model';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css']
})
export class ChatsComponent implements OnInit {

  constructor(private buddyService: BuddyService, private messageService: MessageService) { }

  chats: Message[];
  ngOnInit() {
    this.messageService.getMessages({chats:true}).subscribe(
      data => { this.chats = data },
      err => console.error(err),
    );
  }

}
