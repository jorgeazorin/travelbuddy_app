import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
  router: Router;
  constructor( router: Router) { 
    this.router = router;
  }

  urlsHidden=[];
  
  ngOnInit() {
    this.urlsHidden['/profile/BeBuddy'] = true;
  }


}
