import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileBeBuddyComponent } from './profile-be-buddy.component';

describe('ProfileBeBuddyComponent', () => {
  let component: ProfileBeBuddyComponent;
  let fixture: ComponentFixture<ProfileBeBuddyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileBeBuddyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileBeBuddyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
