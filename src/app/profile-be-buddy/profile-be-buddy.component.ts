import { Component, OnInit } from '@angular/core';
import {MatChipInputEvent} from '@angular/material';
import {ENTER, COMMA} from '@angular/cdk/keycodes';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Buddy from '../../models/buddy.model';
import swal from 'sweetalert2'
import { BuddyService } from '../../services/buddy.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-be-buddy',
  templateUrl: './profile-be-buddy.component.html',
  styleUrls: ['./profile-be-buddy.component.css']
})
export class ProfileBeBuddyComponent implements OnInit {

  constructor(private router: Router, private buddyService: BuddyService) { }

  ngOnInit() {
  }

  loading : Boolean = false;
  marker: any= {
    lat: 38.38644876261229,
    lng: -0.5099144127115096,
  };

  mapClicked($event: any) {
    this.marker = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
    };
  }

  beBuddyForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('')

  });


  beBuddy (){
    let buddy = new Buddy()
    buddy.publicName = this.beBuddyForm.value.name
    buddy.description = this.beBuddyForm.value.description
    buddy.hobbies = this.aficiones.toString()
    buddy.location = JSON.stringify(this.marker);
    this.loading = true;
    this.buddyService.createBuddy(buddy, this.selectedFile).subscribe(
      data => {
        swal({
          type: 'success',
          title: '¡Bien!',
          text: '¡Te has hecho Buddy correctamente!'
        })
        this.loading = false;

        this.router.navigateByUrl('/home');
        
      },err =>{
        console.error(err);
        swal({
          type: 'error',
          title: 'Oops...',
          text: '¡Ha habido un error con el registro!'
        })

        this.loading = false;
        
      }
    );
  }

  selectedFile: File;

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];

    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      var img :any= document.querySelector("#preview img");
      img.file = file;
      var reader = new FileReader();
      reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
      reader.readAsDataURL(file);

    }
  
   }






  /*
      AFICIONES
  */
  separatorKeysCodes = [ENTER, COMMA];
  aficiones = [];
  add(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;
    if ((value || '').trim()) {
      this.aficiones.push( value.trim() );
    }
    if (input) {
      input.value = '';
    }
  }
  remove(fruit: any): void {
    let index = this.aficiones.indexOf(fruit);
    if (index >= 0) {
      this.aficiones.splice(index, 1);
    }
  }





}
