import { Component, OnInit } from '@angular/core';
import { BuddyService } from '../../services/buddy.service';
import Buddy from '../../models/buddy.model';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from '../../services/comment.service';
import Comment  from '../../models/comment.model';
import User from '../../models/user.model';
@Component({
  selector: 'app-view-buddy',
  templateUrl: './view-buddy.component.html',
  styleUrls: ['./view-buddy.component.css']
})
export class ViewBuddyComponent implements OnInit {

  constructor(private route:  ActivatedRoute,private buddyService: BuddyService, private commentService: CommentService) { }

  id: number;
  buddy : Buddy;
  comments: Comment[];
  newComment: String;
  loaded: Boolean = false;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; 

      this.buddyService.getBuddies({userId:this.id}).subscribe(
        data => { 
          this.buddy = data[0]; 
          this.loaded = true 
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );

      this.buddyService.getComments(this.id).subscribe(
        data=>this.comments = data
      )

   });
  }

  guardarComentario(){
    let comment: Comment = new Comment();
    console.log(this.newComment)
    comment.comment= this.newComment;
    comment.buddy = new Buddy();
    comment.buddy.user = new User();
    comment.buddy.user.id = this.id.toString();


    this.commentService.createComment(comment).subscribe(
      data=>{
        this.buddyService.getComments(this.id).subscribe(
          data=>this.comments = data
        )
      }
    )
  }

}
