import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBuddyComponent } from './view-buddy.component';

describe('ViewBuddyComponent', () => {
  let component: ViewBuddyComponent;
  let fixture: ComponentFixture<ViewBuddyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBuddyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBuddyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
