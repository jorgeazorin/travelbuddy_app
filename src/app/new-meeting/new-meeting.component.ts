import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MeetingService } from '../../services/meeting.service';
import Meeting from '../../models/meeting.model';
import User from '../../models/user.model';
import swal from 'sweetalert2'
import { Router, ActivatedRoute } from '@angular/router';
import Buddy from '../../models/buddy.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-meeting',
  templateUrl: './new-meeting.component.html',
  styleUrls: ['./new-meeting.component.css']
})
export class NewMeetingComponent implements OnInit {


  meeting: Meeting = new Meeting();
  loaded: Boolean = false
  idBuddy: Number;
  marker: any= {
    lat: 38.38644876261229,
    lng: -0.5099144127115096,
  };

  mapClicked($event: any) {
    this.marker = {
      lat: $event.coords.lat,
      lng: $event.coords.lng,
    };
  }



  newMeetingForm = new FormGroup({
    start: new FormControl('', [Validators.required]),
    end: new FormControl('', [Validators.required]),
    message:new FormControl('')
  })


  
  
  constructor(private meetingService: MeetingService, private userService: UserService,private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    //this.session = JSON.parse(localStorage.getItem('session'));    
    this.route.params.subscribe(params => {
      this.idBuddy = +params['id'];
      var id = +params['id'];   
      
          this.userService.getUsers({id:id}).subscribe(
            data => {
              this.meeting.buddy = new Buddy();
              this.meeting.buddy.user = data[0];
              this.loaded = true;
            }
          )
        }
      )
  }




  create = function() {

    this.meeting.start = this.newMeetingForm.value.start;
    this.meeting.end = this.newMeetingForm.value.end;
    this.meeting.location = JSON.stringify(this.marker);
    this.meeting.message = this.newMeetingForm.value.message;


    
    this.meetingService.createMeeting(this.meeting).subscribe(
      data => {
        this.meeting = data
        swal({
          type: 'success',
          title: '¡Bien!',
          text: '¡Has organizado la quedada correctamente!'
        })

        this.router.navigateByUrl('/meetings');
      }
    )
  }








}
