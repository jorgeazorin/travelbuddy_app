import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TravelBuddy';

  constructor() {
    if(!localStorage.getItem('session') && !window.location.href.includes("login")){
      window.location.href="/login";
    }
   }

  ngOnInit() {
    
  }
}
