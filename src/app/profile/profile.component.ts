import { Component, OnInit } from '@angular/core';
import Session from '../../models/session.model';
import User from '../../models/user.model';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private route: Router) { }

   session :Session;
  ngOnInit() {
    this.session = JSON.parse(localStorage.getItem('session'));
  }

  logOut(){
    localStorage.removeItem('session');
    this.route.navigateByUrl('/login');
  }

}
