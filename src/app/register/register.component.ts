import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import swal from 'sweetalert2'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import User from '../../models/user.model';
import { Router } from '@angular/router';
import { CustomValidators } from '../../helpers/customValidators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }


  registerForm = new FormGroup({
    email: new FormControl('', [Validators.required,Validators.email]),
    password: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    lastname:new FormControl('', [Validators.required]),
    passwordRepeated :new FormControl('', [Validators.required]),
    birth:new FormControl('', [Validators.required])
  }, CustomValidators.Match('password', 'passwordRepeated'));

  register(){

    var user : User = new User;

    user.email = this.registerForm.value.email;
    user.password = this.registerForm.value.password;
    user.name = this.registerForm.value.name;
    user.lastname  = this.registerForm.value.lastname;
    user.birth  = this.registerForm.value.birth;

    this.userService.createUser(user).subscribe(
      data => {
        swal({
          type: 'success',
          title: '¡Bien!',
          text: '¡Te has registrado correctamente!'
        })

        this.router.navigateByUrl('/login');
        
      },err =>{
        console.error(err);
        swal({
          type: 'error',
          title: 'Oops...',
          text: '¡Ha habido un error con el registro!'
        })

      }
    );
  }


}
