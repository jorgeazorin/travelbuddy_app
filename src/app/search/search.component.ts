import { Component, OnInit } from '@angular/core';
import User from '../../models/user.model';
import { BuddyService } from '../../services/buddy.service';
import Buddy from '../../models/buddy.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

   buddies: Buddy[];
   name;

  constructor(private buddyService: BuddyService, private router: Router) { }

  parse(a) {
    if(a){
      return JSON.parse(a);
    }else{
      return {lat:0, lng:0}    
    }
  }

  ngOnInit() {
    this.buddyService.getBuddies({}).subscribe(
      data => { this.buddies = data },
      err => console.error(err),
    );
  }



  clickedMarker(buddy){
    this.router.navigate(['/buddies/',buddy.user.id]);
  }

}
