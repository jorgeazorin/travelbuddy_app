import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { BuddyService } from '../../services/buddy.service';
import { UserService } from '../../services/user.service';
import User from '../../models/user.model';
import Message from '../../models/message.model';
import Session from '../../models/session.model';

@Component({
  selector: 'app-chats-row',
  templateUrl: './chats-row.component.html',
  styleUrls: ['./chats-row.component.css']
})
export class ChatsRowComponent implements OnInit {

  @Input() 
  message: Message;

  session :Session;
  user: User;
  constructor( private buddyService: BuddyService, private userService: UserService) { }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges) {
    this.session = JSON.parse(localStorage.getItem('session'));
    this.user = new User();
    if(this.message.from.id == this.session.user.id){
      this.user.id = this.message.to.id;
    }else{
      this.user.id = this.message.from.id
    }


    this.userService.getUsers({id: this.user.id}).subscribe(
      data => { this.user = data[0] }
    );
    
  }
  

}
