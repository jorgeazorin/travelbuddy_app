import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatsRowComponent } from './chats-row.component';

describe('ChatsRowComponent', () => {
  let component: ChatsRowComponent;
  let fixture: ComponentFixture<ChatsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
