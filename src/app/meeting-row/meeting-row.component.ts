import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import Message from '../../models/message.model';
import Session from '../../models/session.model';
import { UserService } from '../../services/user.service';
import { BuddyService } from '../../services/buddy.service';
import User from '../../models/user.model';
import Meeting from '../../models/meeting.model';

@Component({
  selector: 'app-meeting-row',
  templateUrl: './meeting-row.component.html',
  styleUrls: ['./meeting-row.component.css']
})
export class MeetingRowComponent implements OnInit {



  @Input() 
  meeting: Meeting;
  private session :Session;
  private user: User;
  loading: Boolean = true

  constructor( private buddyService: BuddyService, private userService: UserService) { }
 
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.session = JSON.parse(localStorage.getItem('session'));
    this.user = new User();
    if(this.meeting.user.id == this.session.user.id){
      this.user.id = this.meeting.buddy.user.id;
    }else{
      this.user.id = this.meeting.user.id
    }


    this.userService.getUsers({id: this.user.id}).subscribe(
      data => { 
        this.user = data[0] 
        this.loading = false
      }
    );
    
  }

}
