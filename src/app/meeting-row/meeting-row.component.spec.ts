import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingRowComponent } from './meeting-row.component';

describe('MeetingRowComponent', () => {
  let component: MeetingRowComponent;
  let fixture: ComponentFixture<MeetingRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
