import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { TopbarComponent } from './topbar/topbar.component';
import { LeftbarComponent } from './leftbar/leftbar.component';
import { FullPageLayoutComponent } from './full-page-layout/full-page-layout.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { MatIconModule} from '@angular/material/icon';
import { SearchComponent } from './search/search.component'
import {HttpModule, Http, XHRBackend, RequestOptions} from '@angular/http';
import {UserService} from '../services/user.service'
import {BuddyService} from '../services/buddy.service'
import { httpFactory } from '../services/http.factory';
import 'rxjs/add/operator/map';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { ProfileBeBuddyComponent } from './profile-be-buddy/profile-be-buddy.component';
import {MatChipsModule} from '@angular/material/chips';
import { ImageEnvironment } from '../helpers/imageEnvironment.component';
import { ViewBuddyComponent } from './view-buddy/view-buddy.component';
import { ChatsComponent } from './chats/chats.component';
import { MessageService } from '../services/message.service';
import { ChatComponent } from './chat/chat.component';
import { ChatsRowComponent } from './chats-row/chats-row.component';
import { MomentModule } from 'ngx-moment';
import { MeetingService } from '../services/meeting.service';
import { MeetingRowComponent } from './meeting-row/meeting-row.component';
import { MeetingsComponent } from './meetings/meetings.component';
import { MeetingComponent } from './meeting/meeting.component';
import { NewMeetingComponent } from './new-meeting/new-meeting.component';
import {RatingModule} from 'ngx-rating';
import { CommentService } from '../services/comment.service';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

const appRoutes: Routes = [
  {
    path: 'register',
    component: RegisterComponent,
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },

  {
    path: '',
    component: FullPageLayoutComponent,
    children: [
      { path: '', redirectTo: '/home',  pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'profile/BeBuddy', component: ProfileBeBuddyComponent },
      { path: 'search', component: SearchComponent },
      { path: 'buddies/:id', component: ViewBuddyComponent },
      { path: 'chats', component: ChatsComponent },
      { path: 'chats/:id', component: ChatComponent },
      { path: 'meetings/:id', component: MeetingComponent },
      { path: 'meetings', component: MeetingsComponent },
      { path: 'meetings/new/:id', component: NewMeetingComponent },      


      { path: '**', 
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  }

];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TopbarComponent,
    LeftbarComponent,
    FullPageLayoutComponent,
    ProfileComponent,
    RegisterComponent,
    SearchComponent,
    BottomBarComponent,
    ProfileBeBuddyComponent,
    ImageEnvironment,
    ViewBuddyComponent,
    ChatsComponent,
    ChatComponent,
    ChatsRowComponent,
    MeetingRowComponent,
    MeetingsComponent,
    MeetingComponent,
    NewMeetingComponent,
  ],
  imports: [
    NgbModule.forRoot(),FormsModule,FormsModule, MatDividerModule, MatChipsModule,MomentModule, RatingModule,
    ReactiveFormsModule,  BrowserModule, MatSidenavModule,MatButtonModule,  MatCheckboxModule,MatInputModule,AgmJsMarkerClustererModule,
    BrowserAnimationsModule, MatIconModule,  RouterModule.forRoot(appRoutes), HttpModule, AgmCoreModule.forRoot({apiKey: 'AIzaSyDtYzf7pP7pdjJ-4zQJXZuQRoc0WvINLFs'})
  ],
  providers: [UserService, BuddyService, MessageService, MeetingService, CommentService, 
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
