import User from "./user.model";

class Session {
    token:string;
    calendar: Date;
    user: User;



    constructor(){
        this.token= ""
        this.calendar = new Date()
        this.user = new User
        
    }

    public getUser(){return this.user}
    public getToken(){return this.token}
    public getCalendar(){return this.calendar}

}

export default Session;