import Meeting from "./meeting.model";

class MeetingImage {
    id:number;
    name: string;
    description: string;
    meeting: Meeting;
    date: Date;



    constructor(){
        
    }

}

export default MeetingImage;