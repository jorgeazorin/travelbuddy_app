import User from "./user.model";

class Buddy {
    user:User;
    publicName: string;
    description: string;
    hobbies: string;
    location: string;
    points: number;



    constructor(){
        this.user = new User()
        this.publicName = ""
        this.description = ""
        this.hobbies = ""
        this.location = ""
    }

}

export default Buddy;