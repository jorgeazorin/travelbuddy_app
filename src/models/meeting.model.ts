import User from "./user.model";
import Buddy from "./buddy.model";

class Meeting {
    id:number;
    user: User;
    buddy: Buddy;
    start: Date;
    end: Date;
    location: string;
    rate: number;
    constructor(){
        
    }

}

export default Meeting;