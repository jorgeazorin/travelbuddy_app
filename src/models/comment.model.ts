import User from "./user.model";
import Buddy from "./buddy.model";

class Comment {
    user:User;
    buddy: Buddy;
    comment: String;
    date: Date;
    id: Number;


}

export default Comment;