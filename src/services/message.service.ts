import Message from '../models/message.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response, Http} from '@angular/http';
import { Injectable } from '@angular/core';
import Session from '../models/session.model';


@Injectable()
export class MessageService {
   

  constructor( private http: Http ) { }


  getMessages(params): Observable<Message[]>{
    return this.http.get('messages?'+ Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')).map(res  => {
      return res.json() as Message[];
    })
  }

  createMessage(message: Message): Observable<Message>{
    return this.http.post('messages', message).map(res  => {
      return res.json() as Message;
    })
  }





}