import Buddy from '../models/buddy.model';
import Comment from '../models/comment.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Response, Http, RequestOptionsArgs, RequestOptions, Headers} from '@angular/http';
import { Injectable } from '@angular/core';


@Injectable()
export class BuddyService {
   

  constructor( private http: Http ) { }


  getComments(id:Number): Observable<Comment[]>{
    return this.http.get('buddies/comments/'+id).map(res  => {
      return res.json() as Comment[];
    })
  }


  getBuddies(params): Observable<Buddy[]>{
    return this.http.get('buddies?'+ Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')).map(res  => {
      return res.json() as Buddy[];
    })
  }

  createBuddy(buddy: Buddy, file: File): Observable<Buddy>{
    let formData = new FormData();
    formData.append("photo", file)
    formData.append('buddy', JSON.stringify( buddy))

    let options: RequestOptionsArgs = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Accept', 'application/json');

    console.log(options)
    return this.http.post('buddies', formData, options).map(res  => {
      return res.json() as Buddy;
    })
  }

  updateBuddy(buddy: Buddy): Observable<Buddy>{
    return this.http.put('buddies', buddy).map(res  => {
      return res.json() as Buddy;
    })
  }



}