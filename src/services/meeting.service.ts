import Meeting from '../models/meeting.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Response, Http, RequestOptionsArgs, RequestOptions, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
import Session from '../models/session.model';
import MeetingImage from '../models/meetingImage.model';


@Injectable()
export class MeetingService {
   

  constructor( private http: Http ) { }


  getMeetings(params): Observable<Meeting[]>{
    return this.http.get('meetings?'+ Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')).map(res  => {
      return res.json() as Meeting[];
    })
  }

  createMeeting(meeting: Meeting): Observable<Meeting>{
    return this.http.post('meetings', meeting).map(res  => {
      return res.json() as Meeting;
    })
  }

  updateMeeting(meeting: Meeting): Observable<Meeting>{
    return this.http.put('meetings', meeting).map(res  => {
      return res.json() as Meeting;
    })
  }


  deleteMeeting(id: number): Observable<Meeting>{
    return this.http.delete('meetings/'+id).map(res  => {
      return res.json() as Meeting;
    })
  }



  updateSocore(meeting: Meeting): Observable<Meeting>{
    return this.http.put('meetings/score', meeting).map(res=>{
      return res.json() as Meeting;
    })
  }
  

  getMeetingImages(meeting: number): Observable<MeetingImage[]>{
    return this.http.get('meetings/images/'+meeting).map(res =>{
      return res.json() as MeetingImage[]
    })
  }



  
  uploadMeetingImage(meetingImage: MeetingImage, file: File): Observable<MeetingImage>{
    let formData = new FormData();
    formData.append("image", file)
    formData.append('meetingImage', JSON.stringify( meetingImage))

    let options: RequestOptionsArgs = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Accept', 'application/json');

    return this.http.post('meetings/image', formData, options).map(res  => {
      return res.json() as MeetingImage;
    })
  }




}