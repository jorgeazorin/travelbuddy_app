import User from '../models/user.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response, Http} from '@angular/http';
import { Injectable } from '@angular/core';
import Session from '../models/session.model';


@Injectable()
export class UserService {
   

  constructor( private http: Http ) { }


  createUser(user: User): Observable<User>{
    return this.http.post('users/register', user).map(res  => {
      return res.json() as User;
    })
  }


  login(user: User): Observable<Session>{
    return this.http.post('users/login', user).map(res  => {
      return res.json() as Session;
    })
  }


  updateUser(user: User): Observable<User>{
    return this.http.put('users', user).map(res  => {
      return res.json() as User;
    })
  }

  getUsers(params): Observable<User[]>{
    return this.http.get('users?'+ Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')).map(res  => {
      return res.json() as User[];
    })
  }



}