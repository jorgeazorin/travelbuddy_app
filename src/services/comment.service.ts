import Buddy from '../models/buddy.model';
import Comment from '../models/comment.model';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Response, Http, RequestOptionsArgs, RequestOptions, Headers} from '@angular/http';
import { Injectable } from '@angular/core';


@Injectable()
export class CommentService {
   

  constructor( private http: Http ) { }


  createComment(comment: Comment): Observable<Comment>{
    return this.http.post('comments', comment).map(res  => {
      return res.json() as Comment;
    })
  }


  deleteComment(id: Number): Observable<Comment>{
    return this.http.delete('commments/'+id).map(res  => {
      return res.json() as Comment;
    })
  }



}