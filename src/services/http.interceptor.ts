import {Injectable} from "@angular/core";
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {environment} from "../environments/environment";

@Injectable()
export class InterceptedHttp extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }
    
    errorInterceptor(error) {
        console.log("ups jeje", error);
        if(error.status===401){
            window.location.href = '/login';
        }
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        let a = super.request(url, options)
        a.subscribe(
            data => {},
            this.errorInterceptor
        );
        return a;
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        let a = super.get(url, this.getRequestOptionArgs(options));
        a.subscribe(
            data => {},
            this.errorInterceptor
        );
        return a;
    }

    post(url: string, body: string | FormData, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        let a = super.post(url, body, this.getRequestOptionArgs(options));
        a.subscribe(
            data => {},
            this.errorInterceptor
        );
        return a;
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        let a = super.put(url, body, this.getRequestOptionArgs(options));
        a.subscribe(
            data => {},
            this.errorInterceptor
        );
        return a;
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        let a = super.delete(url, this.getRequestOptionArgs(options));
        a.subscribe(
            data => {},
            this.errorInterceptor
        );
        return a;
    }

    private updateUrl(req: string) {
        return  environment.origin + req;
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
            options.headers.append('Content-Type', 'application/json');
        }
        
        if(JSON.parse(localStorage.getItem('session')))
            options.headers.append('Authorization', JSON.parse(localStorage.getItem('session')).token);
        return options;
    }
}