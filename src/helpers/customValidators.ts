import {AbstractControl} from '@angular/forms';

export class CustomValidators {

  /**
   * Match two controls if they are the same
   * @param firstControlName
   * @param secondControlName
   * @returns {(AC: AbstractControl) => any}
   * @constructor
   */
  static Match(firstControlName, secondControlName) {
    return (AC: AbstractControl) => {
      let firstControlValue = AC.get(firstControlName).value; // to get value in input tag
      let secondControlValue = AC.get(secondControlName).value; // to get value in input tag
      if (firstControlValue != secondControlValue) {
        if(!AC.get(secondControlName).errors){
          AC.get(secondControlName).setErrors ( {MatchFields: true});
        }else{
          AC.get(secondControlName).setErrors(AC.get(secondControlName).errors.push({MatchFields: true}));
        }
      } else {
        return null
      }
    };
  }
}