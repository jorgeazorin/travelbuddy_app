import { Component, OnInit, Input, ElementRef, Renderer, SimpleChanges } from '@angular/core';
import {environment} from "../environments/environment";


@Component({
    selector: '[imageEnvironment]',
    template: `  `
})
export class ImageEnvironment implements OnInit {

    @Input() url: string;

    constructor (private _elRef: ElementRef, private _renderer: Renderer) { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        this._renderer.setElementAttribute(this._elRef.nativeElement, 'src', environment.origin+ this.url);    
    }
  
  }
  